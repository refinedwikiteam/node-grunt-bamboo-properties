module.exports = function(grunt) {

  var testCustomKeys = require('./testCustomKeys.json');
  var testAll = require('./testAll.json');
  var testSelected = require('./testSelected.json');
  grunt.initConfig({
    'bamboo-properties': {
      main: {
        outputFile: 'properties.txt',
        jsonFiles: [
          {
            contents: testCustomKeys,
            properties: {
              version: 'test.sub'
            }
          },
          {
            contents: testAll,
            properties: '*'
          },
          {
            contents: testSelected,
            properties:['testSelected']
          }
        ]
      }
    }
  });
  grunt.loadTasks('tasks');

  grunt.registerTask('default', ['bamboo-properties']);
};