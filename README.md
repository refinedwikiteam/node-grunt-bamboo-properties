# grunt-bamboo-properties

Converts JSON configuration files into a properties format that works with Atlassian Bamboo's "Inject Bamboo Variables" task.

## Getting Started
This plugin requires Grunt `>=0.4.0`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```
$ npm install --save-dev grunt-bamboo-properties
```

## Usage

```js
require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks

grunt.initConfig({
    'bamboo-properties': {
         main: {
            outputFile: 'properties.txt',
            jsonFiles: [
              {
                contents: testCustomKeys,
                properties: {
                  version: 'test.sub'
                }
              },
              {
                contents: testAll,
                properties: '*'
              },
              {
                contents: testSelected,
                properties:['testSelected']
              }
            ]
          }
    }
});

grunt.registerTask('default', ['bamboo-properties']);
```

### Options

#### outputFile
Type: `String`

Default: `Properties.txt`

Specifies the path to the output file

#### jsonFiles
Type: `Array`

The files to be parsed

##### contents
Type: `Object`

The object hash contents of the JSON file

##### properties
Type: `String`, `Array`,`Object`

The properties to load. Can be a string, indicating all properties should be copied, an array of strings with the keys to copy or an object mapping output keys to input keys (Useful for nested keys).
