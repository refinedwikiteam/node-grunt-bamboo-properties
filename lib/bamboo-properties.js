'use strict';
module.exports = function(grunt, self) {
  var fs = require('fs');

  var requiredMainProperties = ['jsonFiles', 'outputFile'];
  var requiredProperties = ['contents', 'properties'];

  let config = self.data;
  if (!config) {
    grunt.fail.fatal('Missing configuration');
    return;
  }
  for (let j = 0; j < requiredMainProperties.length; j++) {
    if (!config.hasOwnProperty(requiredMainProperties[j])) {
      grunt.fail.fatal('Missing property ' + requiredMainProperties[j]);
      return;
    }
  }
  let output = {};
  for (let i = 0; i < config.jsonFiles.length; i++) {
    let jsonFile = config.jsonFiles[i];
    // Check for all required properties, warn and skip if missing
    let missingProp = false;
    for (let j = 0; j < requiredProperties.length; j++) {
      if (!jsonFile.hasOwnProperty(requiredProperties[j])) {
        grunt.log.warn('Missing property ' + requiredProperties[j]);
        missingProp = true;
        break;
      }
    }
    if (missingProp) {
      continue;
    }
    let jsonContents = jsonFile.contents;
    let properties = jsonFile.properties;
    // Three cases, properties can be a * meaning all properties, it can be an object mapping keys in the output to
    // keys in the input, or it can be an array of properties to copy using the same keys
    // Case 1, it's an array
    if (properties instanceof  Array) {
      for (let j = 0; j < properties.length; j++) {
        let property = properties[j];
        output[property] = jsonContents[property];
      }
    } else if (typeof properties === 'object') { // Case 2, it's an object
      let recurseDots = function(property, contents) {
        if (property.indexOf('.') !== -1) {
          let newProperty = property.substr(0, property.indexOf('.'));
          return recurseDots(property.substr(property.indexOf('.') + 1, property.length), contents[newProperty]);
        } else {
          return contents[property];
        }
      };
      for (let outputKey in properties) {
        let inputKey = properties[outputKey];
        output[outputKey] = recurseDots(inputKey, jsonContents);
      }
    } else {
      for (let key in jsonContents) {
        output[key] = jsonContents[key];
      }
    }
  }
  var outString = '';
  for (var obj in output) {
    if (output.hasOwnProperty(obj)) {
      outString += obj + '=' + output[obj] + '\n';
    }
  }
  let outputfile = config.outputFile || 'properties.txt';
  fs.writeFileSync(outputfile, outString);
  grunt.log.ok('Successfully generated Bamboo properties.');
};