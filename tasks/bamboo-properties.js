'use strict';
module.exports = function(grunt) {
  var fs = require('fs');
  var bamboo = require('../lib/bamboo-properties');

  grunt.registerMultiTask('bamboo-properties',
    'Generates a Bamboo-friendly properties file from JSON files',
    function() {
      bamboo(grunt, this);
    });
};