'use strict';
var expect = require('chai').expect;
var bamboo = require('../lib/bamboo-properties');
var fs = require('fs');
var grunt;

describe('Properties', function() {
  beforeEach(function() {
    grunt = {};
    grunt.fail = {};
    grunt.fail.fatal = function(log) {
      console.error('Task is failing: ' + log);
    };

    grunt.log = {};
    grunt.log.ok = function(log) {
    };

    if (fs.existsSync('properties.txt')) {
      fs.unlinkSync('properties.txt');
    }
  });

  afterEach(function() {
    if (fs.existsSync('properties.txt')) {
      fs.unlinkSync('properties.txt');
    }
  });

  it('should be empty if no files are given', function() {
    let config = {
      jsonFiles: [],
      outputFile: 'properties.txt'
    };
    let self = {};
    self.data = config;
    bamboo(grunt, self);
    expect(fs.existsSync('properties.txt')).to.equal(true);
    expect(fs.readFileSync('properties.txt', {encoding: 'utf8'})).to.equal('');
  });

  it('should contain all properties when all are specified', function() {
    let config = {
      jsonFiles: [
        {
          contents: require('../testAll.json'),
          properties: '*'
        }
      ],
      outputFile: 'properties.txt'
    };
    let self = {};
    self.data = config;
    bamboo(grunt, self);
    expect(fs.existsSync('properties.txt')).to.equal(true);
    expect(fs.readFileSync('properties.txt', {encoding: 'utf8'})).to.equal('all1=all1\nall2=all2\n');
  });

  it('should contain only select properties when certain props are specified', function() {
    let config = {
      jsonFiles: [
        {
          contents: require('../testSelected.json'),
          properties: ['testSelected']
        }
      ],
      outputFile: 'properties.txt'
    };
    let self = {};
    self.data = config;
    bamboo(grunt, self);
    expect(fs.existsSync('properties.txt')).to.equal(true);
    expect(fs.readFileSync('properties.txt', {encoding: 'utf8'})).to.equal('testSelected=Should output\n');
  });

  it('should contain properties with custom key when custom keys are defined', function() {
    let config = {
      jsonFiles: [
        {
          contents: require('../testCustomKeys.json'),
          properties: {
            version: 'test.sub'
          }
        }
      ],
      outputFile: 'properties.txt'
    };
    let self = {};
    self.data = config;
    bamboo(grunt, self);
    expect(fs.existsSync('properties.txt')).to.equal(true);
    expect(fs.readFileSync('properties.txt', {encoding: 'utf8'})).to.equal('version=1.1.2\n');
  });
});